package ru.sctbelpa.meteostation_service;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SensorsDataKeeper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "measures.db";
	private static final int DATABASE_VERSION = 1;
	
	// столбцы
	public static final String UID = "_id";
	public static final String Date = "Date";
	public static final String P = "Pressure";
	public static final String T = "Temperature";
	public static final String h = "Humidity";
	
	public static final String _D = "D";
	
	public static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
			+ "%s" + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ Date + " INTEGER, " + P + " REAL, " + T + " REAL, "
			+ h + " REAL);";

	public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS %s";
	
	private final BluetoothDevice device;
	
	public SensorsDataKeeper(Context context, BluetoothDevice dev)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		device = dev;
	}
	
	public String tableName()
	{
		return _D + Integer.toHexString(device.getAddress().hashCode());
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(String.format(SQL_CREATE_ENTRIES, 
				tableName()));
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("LOG_TAG", "Обновление базы данных с версии " + oldVersion
				+ " до версии " + newVersion + ", которое удалит все старые данные");

		db.execSQL(String.format(SQL_DELETE_ENTRIES, tableName()));
		onCreate(db);
	}	
}
