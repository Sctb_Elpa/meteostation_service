package ru.sctbelpa.meteostation_service.BT;

import java.util.ArrayList;
import java.util.List;
import android.bluetooth.BluetoothDevice;
import android.util.SparseArray;
import android.widget.Adapter;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class BTItemContent {

	
	public static Adapter adapter;
	/**
	 * An array of sample (dummy) items.
	 */
	public static List<BTItem> ITEMS = new ArrayList<BTItem>();

	/**
	 * A map of sample (dummy) items, by ID.
	 */
	public static SparseArray<BTItem> ITEM_MAP = new SparseArray<BTItem>();
	
	public static void clearItems()
	{
		ITEMS.clear();
		ITEM_MAP.clear();
	}
	
	public static void addItem(BTItem item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}

	/**
	 * A dummy item representing a piece of content.
	 */
	public static class BTItem {
		public int id;
		public BluetoothDevice device;

		public BTItem(int id, BluetoothDevice device) {
			this.id = id;
			this.device = device;
		}

		@Override
		public String toString() {
			return device.getName() + " (" + device.getAddress() + ")";
		}
	}
}
