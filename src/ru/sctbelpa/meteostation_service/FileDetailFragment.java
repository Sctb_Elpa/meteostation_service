package ru.sctbelpa.meteostation_service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.sctbelpa.meteostation_service.FsModel.FSContent;
import ru.sctbelpa.meteostation_service.service.IOService;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A fragment representing a single File detail screen. This fragment is either
 * contained in a {@link FileListActivity} in two-pane mode (on tablets) or a
 * {@link FileDetailActivity} on handsets.
 */
public class FileDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_NAME = "ru.sctbelpa.meteostation_service.name";
	public static final String DIRECTORY_NAME = "ru.sctbelpa.meteostation_service.Dir";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private FSContent.FsObject mItem;

	private ServiceConnection sConn;
	private Intent intent;
	public IOService.IOServiceBinder bndr = null;

	private View rootView;

	private Date day = null;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public FileDetailFragment() {
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Bundle arguments = getArguments();

		if (arguments.containsKey(ARG_ITEM_NAME)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = FSContent.ITEM_MAP.get(arguments.getString(
					ARG_ITEM_NAME));
		}

		intent = new Intent(getActivity(), IOService.class);
		intent.putExtra(IOService.DEV_MAC, 
				arguments.getString(IOService.DEV_MAC));

		final String directory = arguments.getString(DIRECTORY_NAME);

		if (mItem != null)
		{
			if (directory.contains("ONESHOT"))
			{
				Pattern p = Pattern.compile("(\\d\\d)(\\d\\d)(\\d\\d\\d\\d).CSV");
				Matcher m = p.matcher(mItem.name);
				if (m.find())
					day = new Date(
							Integer.parseInt(m.group(3)),
							Integer.parseInt(m.group(2)),
							Integer.parseInt(m.group(1))
							);
			}
			else if (directory.contains("SERIES")) 
			{
				Pattern p = Pattern.compile("\\SERIES\\/(\\d\\d)(\\d\\d)(\\d\\d\\d\\d)");
				Matcher m = p.matcher(directory);
				if (m.find())
					day = new Date(
							Integer.parseInt(m.group(3)),
							Integer.parseInt(m.group(2)),
							Integer.parseInt(m.group(1))
							);
			} 
		}
		
		if (day == null)
			day = new Date();

		sConn = new ServiceConnection() {

			@Override
			public void onServiceConnected(ComponentName name, IBinder binder) {
				bndr = (IOService.IOServiceBinder)binder;

				if (mItem != null) 
				{
					if (!mItem.isFolder)
					{
						ReadHistoryFile r = new ReadHistoryFile(FileDetailFragment.this, 
								"cat " + mItem.name, 
								(LinearLayout)rootView.findViewById(R.id.graph_history));				
						r.execute();
					}
				}
			}

			@Override
			public void onServiceDisconnected(ComponentName arg0) {
				// TODO: ����� ����� � ��������
				bndr = null;
			}
		};

		getActivity().bindService(intent, sConn, Activity.BIND_AUTO_CREATE);
	}

	@SuppressWarnings("deprecation")
	public List<Dataset> parseCSV(List<String> csv)
	{
		final String header = "Time;Temperature [*C];Pressure [mmHg];Humidity [%];Charge [%]";
		List<Dataset> result = new ArrayList<Dataset>();
		try
		{
			if (csv.get(0).equals(header))
			{
				csv.remove(0);
				for (String line : csv) {
					Matcher m = DisplayDetailFragment.pattern.matcher(line);
					if (m.find())
					{
						Date timestamp = (Date) day.clone();
						timestamp.setHours(Integer.parseInt(m.group(1)));
						timestamp.setMinutes(Integer.parseInt(m.group(2)));
						timestamp.setSeconds(Integer.parseInt(m.group(3)));

						Dataset item = new Dataset(
								timestamp.getTime(),
								Float.parseFloat(m.group(4)),
								Float.parseFloat(m.group(5)),
								Float.parseFloat(m.group(6))
								);
						result.add(item);
					}
				}
			}
		}
		catch (Exception e)
		{
			Log.e("parser", e.getMessage());
		}

		return result;
	}

	public void DrawGraph(List<Dataset> dataset)
	{
		LinearLayout place = (LinearLayout)rootView.findViewById(R.id.graph_history);
		place.addView(graph.buildGraph(dataset, ""));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_file_detail,
				container, false);

		// Show the dummy content as text in a TextView.
		if (mItem != null) 
		{
			((TextView) rootView.findViewById(R.id.Filename_graph))
			.setText(mItem.name);
		}
		return rootView;
	}
}
