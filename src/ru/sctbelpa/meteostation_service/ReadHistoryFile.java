package ru.sctbelpa.meteostation_service;

import java.util.List;

import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

public class ReadHistoryFile extends AsyncTask<Void, Void, Void> {

	private String cmd;
	private LinearLayout pp;
	private FileDetailFragment c;
	private boolean falied = false;
	private List<Dataset> r;

    public ReadHistoryFile(FileDetailFragment c, String command, LinearLayout progressPlace)
    {
        this.cmd = command;
        this.c = c;
        pp = progressPlace;
    }

    @Override
    protected void onPreExecute()
    {
    	Drawable background = new ColorDrawable(0xFF373737);
		Drawable progress = new ColorDrawable(0xFF00B51C);
		ClipDrawable clipProgress = new ClipDrawable(progress, Gravity.LEFT,
				ClipDrawable.HORIZONTAL);

		// ���� ��� ����������
		LayerDrawable layerlist = new LayerDrawable(new Drawable[] {
				background, clipProgress });
		layerlist.setId(0, android.R.id.background);
		layerlist.setId(1, android.R.id.progress);
		
		ProgressBar progressBar = new ProgressBar(c.getActivity(), null, 
				android.R.attr.progressBarStyleLarge);
		progressBar.setIndeterminate(true);
		progressBar.setLayoutParams(new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT));
		progressBar.setProgressDrawable(layerlist);
		
		pp.removeAllViews();
		pp.addView(progressBar);
    }

    @Override
    protected Void doInBackground(Void... params)
    {
    	// read file
		List<String> ansver = c.bndr.processCommand(cmd);
		if (ansver == null || ansver.isEmpty())
		{
			falied = true;
			return null;
		}

		// ������ ������
		r = c.parseCSV(ansver);
				
        return null;
    }

    @Override
    protected void onPostExecute(Void result)
    {
    	pp.removeAllViews();
    	
    	if (falied)
    		Toast.makeText(c.getActivity(), 
    				c.getActivity().getResources().getString(R.string.Fread_err), 
    				Toast.LENGTH_SHORT).show();
    	else 
    		c.DrawGraph(r);
    }

}
