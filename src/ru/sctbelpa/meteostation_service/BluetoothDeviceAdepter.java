package ru.sctbelpa.meteostation_service;

import java.util.List;

import ru.sctbelpa.meteostation_service.BT.BTItemContent.BTItem;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BluetoothDeviceAdepter extends BaseAdapter {
	
	private static final String ApplicatableDevNameTemplate = "SCTB_Meteostation";

	private Context context;
	private LayoutInflater inflater;
	private List<BTItem> objects;
	
	public BluetoothDeviceAdepter(Context c, List<BTItem> items)
	{
		context = c;
		objects = items;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		return objects.size();
	}

	@Override
	public Object getItem(int pos) {
		return objects.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View container, ViewGroup parent) {
	
		View view = (container == null) ? 
				(inflater.inflate(R.layout.bt_list_item, parent, false)) : container;
		
		BluetoothDevice d = ((BTItem)getItem(position)).device;
		
		String name = d.getName();
		
		if (name.contains(ApplicatableDevNameTemplate))
			((ImageView)view.findViewById(R.id.device_icon)).setImageResource(R.drawable.ic_launcher);
		
		((TextView)view.findViewById(R.id.name)).setText(name);
		((TextView)view.findViewById(R.id.mac)).setText(d.getAddress());
		return view;
	}
}
