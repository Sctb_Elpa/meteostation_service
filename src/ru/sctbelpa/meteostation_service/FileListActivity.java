package ru.sctbelpa.meteostation_service;

import java.util.List;

import ru.sctbelpa.meteostation_service.FsModel.FSContent;
import ru.sctbelpa.meteostation_service.service.IOService;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

/**
 * An activity representing a list of Files. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link FileDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link FileListFragment} and the item details (if present) is a
 * {@link FileDetailFragment}.
 * <p>
 * This activity also implements the required {@link FileListFragment.Callbacks}
 * interface to listen for item selections.
 */
public class FileListActivity extends FragmentActivity implements
FileListFragment.Callbacks {

	private static final String  DirectorySaveKey = "ru.sctbelpa.meteostation_service.currentPath";
	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;

	private ServiceConnection sConn;
	private Intent intent;
	private boolean bound = false;
	private IOService.IOServiceBinder binder = null;

	private String directory; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_list);

		if (findViewById(R.id.file_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((FileListFragment) getSupportFragmentManager().findFragmentById(
					R.id.file_list)).setActivateOnItemClick(true);
		}

		intent = new Intent(this, IOService.class);
		intent.putExtra(IOService.DEV_MAC, getIntent().getStringExtra("MAC"));

		sConn = new ServiceConnection() {

			@Override
			public void onServiceConnected(ComponentName name, IBinder binder) {
				FileListActivity.this.binder = (IOService.IOServiceBinder)binder;
				bound = true;
				RereadDirectory();
			}

			@Override
			public void onServiceDisconnected(ComponentName arg0) {
				// TODO: ����� ����� � ��������
				FileListActivity.this.binder = null;
				bound = false;
			}
		};

		bindService(intent, sConn, Activity.BIND_AUTO_CREATE);

		if (savedInstanceState!= null)
			directory = savedInstanceState.getString(DirectorySaveKey, "/");
		else 
			directory = "/";
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putString(DirectorySaveKey, directory);
		super.onSaveInstanceState(outState);
	}

	void RereadDirectory()
	{
		// change directory
		List<String> ansver;
		Resources r = getResources();
		if (bound)
		{
			try
			{
				ansver = binder.processCommand("cd " + directory);
				if (ansver == null || ansver.isEmpty())
					throw new RuntimeException(r.getString(R.string.err_cd));
				setTitle(ansver.get(0));

				// ls
				ansver = binder.processCommand("ls");
				if (ansver == null || ansver.isEmpty())
					throw new RuntimeException(r.getString(R.string.err_ls));

				FSContent.clearItems();
				for (String s : ansver) 
				{
					FSContent.addItem(new FSContent.FsObject(s));
				}
				FSContent.adeptor.notifyDataSetChanged();
				return;

			}
			catch (RuntimeException e) 
			{
				Toast.makeText(this, r.getString(R.string.Dread_err) +
						"(" + e.getMessage() + ")", 
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private long back_pressed;

	@Override
	public void onBackPressed() 
	{
		if (back_pressed + 1000 > System.currentTimeMillis()) 
			super.onBackPressed();
		else 
		{
			if (directory.equals("/"))
				super.onBackPressed();
			else
			{
				dirmoveUp();
				RereadDirectory();
			}
		}
		back_pressed = System.currentTimeMillis();
	}

	private void dirmoveUp()
	{
		int slashIndex = directory.lastIndexOf('/');
		if (slashIndex == 0)
			directory = "/";
		else 
			directory = directory.substring(0, slashIndex);
	}

	@Override
	protected void onResume() 
	{
		RereadDirectory();
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		unbindService(sConn);
		super.onDestroy();
	}

	/**
	 * Callback method from {@link FileListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		boolean isFileSelected = false;
		try
		{
			if (FSContent.ITEM_MAP.get(id).isFolder)
			{
				if (id.equals(".."))
					dirmoveUp();
				else
				{
					if (!directory.equals("/"))
						directory += "/";
					directory += id;
				}
				RereadDirectory();
			}
			else
				isFileSelected = true;
		}
		catch (NullPointerException e)
		{
			directory = "/";
			RereadDirectory();
		}

		if (isFileSelected)
		{
			if (mTwoPane) {
				// In two-pane mode, show the detail view in this activity by
				// adding or replacing the detail fragment using a
				// fragment transaction.
				Bundle arguments = new Bundle();
				arguments.putString(FileDetailFragment.ARG_ITEM_NAME, id);
				arguments.putString(IOService.DEV_MAC, intent.getStringExtra(IOService.DEV_MAC));
				arguments.putString(FileDetailFragment.DIRECTORY_NAME, directory);
				FileDetailFragment fragment = new FileDetailFragment();
				fragment.setArguments(arguments);
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.file_detail_container, fragment).commit();

			} else {
				// In single-pane mode, simply start the detail activity
				// for the selected item ID.
				Intent detailIntent = new Intent(this, FileDetailActivity.class);
				detailIntent.putExtra(FileDetailFragment.ARG_ITEM_NAME, id);
				detailIntent.putExtra(IOService.DEV_MAC, intent.getStringExtra(IOService.DEV_MAC));
				detailIntent.putExtra(FileDetailFragment.DIRECTORY_NAME, directory);
				startActivity(detailIntent);
			}
		}
	}
}
