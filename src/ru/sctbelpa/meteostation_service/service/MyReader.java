package ru.sctbelpa.meteostation_service.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.ConcurrentLinkedQueue;

import android.util.Log;

public class MyReader implements Runnable {
	private final InputStreamReader reader;
	public ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<String>();
	private boolean closed = false;
	private String curentStr = new String();

	public MyReader(InputStreamReader reader) {
		this.reader = reader;
	}

	public void run() {
		int readetFails = 10;
		try {
			while (!Thread.currentThread().isInterrupted()) 
			{
				int c;
				try 
				{
					c = reader.read();
				}
				catch (IOException e1) 
				{
					Log.w("MyReader", "read() exception " + e1.getMessage());
					--readetFails;
					if (readetFails == 0)
						break;
					Thread.sleep(1);
					continue;
				}

				readetFails = 10;
				if (c > 0)
				{
					if (((char)c == '\n' || (char)c == '\r'))
					{
						if (!curentStr.isEmpty())
						{
							synchronized (queue)
							{
								queue.add(curentStr);
								queue.notifyAll();
							}
							curentStr = new String();
						}
					}
					else 
						curentStr += (char)c;
				}
			}
		} 
		catch (Exception e) {
			Log.i("MyReader", "Thread closed: " + e.toString());
		}
		closed = true;
	}

	// Returns true iff there is at least one line on the queue
	public boolean ready() {
		return(queue.peek() != null);
	}

	// Returns true if the underlying connection has closed
	// Note that there may still be data on the queue!
	public boolean isClosed() {
		return closed;
	}

	// Get next line
	// Returns null if there is none
	// Never blocks
	public String readLine() {
		return(queue.poll());
	}

	public void flush()
	{
		queue.clear();
	}
}