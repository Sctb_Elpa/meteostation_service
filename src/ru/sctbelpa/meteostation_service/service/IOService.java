package ru.sctbelpa.meteostation_service.service;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Semaphore;

import ru.sctbelpa.meteostation_service.BT.BTItemContent;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class IOService extends Service {
	private static final UUID serialPortUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	public static final String DEV_MAC = "ru.sctbelpa.meteostation_service.DevMAC";
	private static final String LOG_TAG = "IOService";
	private BroadcastReceiver mReceiver;
	private HashMap<String, IOThread> devNumToThreadMap = new HashMap<String, IOThread>();
	boolean isBroadcastRessiverRegistred = false;

	Semaphore semaphore = new Semaphore(1);

	public class IOThread extends Thread 
	{	
		BluetoothSocket socket;
		OutputStream Out;
		MyReader In;
		public String MAC;
		public Object connected = false;

		public IOThread(BluetoothDevice dev) throws IOException
		{		
			MAC = dev.getAddress();
			socket = dev.createInsecureRfcommSocketToServiceRecord(serialPortUUID);
			Out = socket.getOutputStream();
			In = new MyReader(new InputStreamReader(socket.getInputStream()));
		}

		@Override
		public void run() // keepaliveLoop
		{
			try {
				socket.connect();
			} catch (IOException e) {
				Log.e(LOG_TAG, "Failed to open BT socket");
				stopAndUnregisterSelf();
				return;
			}

			Thread readThread = new Thread(In);
			readThread.start();

			synchronized (connected) {
				connected.notifyAll();
			}

			connected = true;

			while(!interrupted())
			{
				try {
					sleep(1000);
					semaphore.acquire();
				} catch (InterruptedException e1) {
					break;
				}

				try {
					Log.d(LOG_TAG, "ping... ");
					Out.write("ping\n\r".getBytes());
					synchronized (In.queue) 
					{
						In.queue.wait(1000);
					}
					if (In.ready())
						In.readLine();
					else
						throw new IOException("No ansver for ping");

				}
				catch (IOException e) 
				{ 
					semaphore.release();
					Log.w(LOG_TAG, "keepalive Thread: " + e.getMessage());
					break; 
				} 
				catch (InterruptedException e)
				{ 
					semaphore.release();
					break;
				}	

				semaphore.release();
			}

			readThread.interrupt();
			try {
				socket.close();
			} catch (IOException e) {}

			Log.i(LOG_TAG, "Poller thread terminated");
			stopAndUnregisterSelf();
		}

		void stopAndUnregisterSelf()
		{
			interrupt();
			for (String key : devNumToThreadMap.keySet())
				if (devNumToThreadMap.get(key).equals(this))
				{
					devNumToThreadMap.remove(key);
					break;
				}					
		}

		List<String> syncRequest(String cmd, int timeout_ms)
		{
			try 
			{
				List<String> result = new ArrayList<String>();

				if (!cmd.endsWith("\n\r"))
					cmd += "\n\r";

				try
				{
					semaphore.acquire();
				}
				catch ( InterruptedException e1)
				{
					return null;
				}

				synchronized (connected) { // ��������� �������� ����������
					if (connected.equals(false))
						connected.wait(2000);
				}

				Out.write(cmd.getBytes());

				int waittime = timeout_ms;
				
				synchronized (In.queue) {
					while (true)
					{
						In.queue.wait(waittime);
						if (In.ready())
							while (In.ready())
							{
								String s = In.readLine();
								if (s.contains("pong"))
								{
									waittime = timeout_ms;
									break;
								}
								result.add(s);
								waittime = timeout_ms / 2;
							}
						else 
							break;	
					}
				}
				semaphore.release();
				return result;
			} 
			catch (Exception e) 
			{
				semaphore.release();
				Log.d(LOG_TAG, "Command execution start: " + cmd);
				return null;
			}
		}
	}

	private void registerRessiver(boolean register)
	{
		if (register)
		{
			if (!isBroadcastRessiverRegistred)
			{
				IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
				filter.addAction(Intent.ACTION_SCREEN_OFF);
				mReceiver = new ScreenReceiver();
				registerReceiver(mReceiver, filter);
			}
		}
		else 
		{
			if (isBroadcastRessiverRegistred)
			{
				unregisterReceiver(mReceiver);
			}
		}
		isBroadcastRessiverRegistred = register;
	}

	private IOThread startPooler(String mac) throws IndexOutOfBoundsException
	{
		int i = 0;
		for (; i < BTItemContent.ITEM_MAP.size(); ++i) 
			if (BTItemContent.ITEM_MAP.valueAt(i).device.getAddress().equals(mac))
				break;
		if (i >= BTItemContent.ITEM_MAP.size())
			throw new IndexOutOfBoundsException(mac + " not in visable devices");

		if (devNumToThreadMap.keySet().contains("" + i))
		{
			return devNumToThreadMap.get("" + i);
		}
		else
		{
			IOThread res;
			try 
			{
				res = new IOThread(BTItemContent.ITEM_MAP.get(i).device);
			} catch (IOException e) 
			{
				Log.e(LOG_TAG, "Failed to spawn pooler thread for " + mac);
				res = null;
			}
			devNumToThreadMap.put("" + i, res);
			res.start();
			return res;
		}		
	}

	public void onCreate() {
		super.onCreate();
		registerRessiver(true);
		Log.d(LOG_TAG, "onCreate");
	}

	public IBinder onBind(Intent intent) {
		Log.d(LOG_TAG, "onBind");
		IOThread pooler;
		String targetMac;
		try
		{
			targetMac = intent.getStringExtra(DEV_MAC);
			pooler = startPooler(targetMac); 
		}
		catch (Exception e) 
		{
			return null;
		}
		return new IOServiceBinder(pooler, targetMac);
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.d(LOG_TAG, "onUnbind");
		String targetMac;
		try
		{
			targetMac = intent.getStringExtra(DEV_MAC);
			for (IOThread t : devNumToThreadMap.values()) 
				if (t.MAC.equals(targetMac))
					t.stopAndUnregisterSelf();
		}
		catch (Exception e) {}
		return super.onUnbind(intent);
	}

	public void onDestroy() {
		Log.d(LOG_TAG, "onDestroy");
		registerRessiver(false);
		super.onDestroy();
	}

	public class IOServiceBinder extends Binder 
	{
		IOThread thread;
		String MAC;

		public IOServiceBinder(IOThread poolerThread, String mac)
		{
			MAC= mac;
			thread = poolerThread;
		}

		public List<String> processCommand(String cmd)
		{
			if (!devNumToThreadMap.values().contains(thread))
			{
				// ������ ��� ����� �����, ������� ������������
				try
				{
					thread = startPooler(MAC);
				}
				catch (IndexOutOfBoundsException e)
				{
					return null;
				}
			}
			return thread.syncRequest(cmd, 500);
		}
	}

	public class ScreenReceiver extends BroadcastReceiver 
	{	 
		@Override
		public void onReceive(Context context, Intent intent) 
		{
			// ��� ������ ����� ������ - �������� ��� ������
			if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) 
			{
				for (String key : devNumToThreadMap.keySet())
					devNumToThreadMap.get(key).stopAndUnregisterSelf();
			}
		}
	}
}
