package ru.sctbelpa.meteostation_service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.sctbelpa.meteostation_service.BT.BTItemContent;
import ru.sctbelpa.meteostation_service.service.IOService;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A fragment representing a single Display detail screen. This fragment is
 * either contained in a {@link DisplayListActivity} in two-pane mode (on
 * tablets) or a {@link DisplayDetailActivity} on handsets.
 */
public class DisplayDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "ru.sctbelpa.meteostation_service._id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private BTItemContent.BTItem mItem = null;

	private ServiceConnection sConn;
	private Intent intent;
	private boolean bound = false;
	private IOService.IOServiceBinder binder;
	private SensorsDataKeeper db;
	private SQLiteDatabase sqdb;
	private View rootView;

	public static final Pattern pattern = 
			Pattern.compile("(\\d+):(\\d+):(\\d+);([-+]?\\d*\\.?,?\\d*);(\\d*\\.?,?\\d*);(\\d*\\.?,?\\d*);(\\d*)");
	private static final Pattern variantsPattern = 
			Pattern.compile("Write interval variants: (\\d+);(\\d+);(\\d+), selected: (\\d+)");

	private OnClickListener refrashClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (bound)
			{
				List<String> ansver = binder.processCommand("current");
				if (ansver != null && ansver.size() != 0)
				{
					String s = ansver.get(0);
					if (addValues(s))
						refreshView();
					else 
						Toast.makeText(getActivity(), 
								R.string.E_ressive_values, Toast.LENGTH_SHORT).show();
					return;
				}
			}
			DisplayNoConnectionAvalable();
		}
	};

	@SuppressWarnings("deprecation")
	public boolean addValues(String data)
	{
		ContentValues measure = new ContentValues();

		Matcher m = pattern.matcher(data);		
		if (!m.find())
			return false;
		Date mesureTimestamp = new Date();
		try
		{
			mesureTimestamp.setHours(Integer.parseInt(m.group(1)));
			mesureTimestamp.setMinutes(Integer.parseInt(m.group(2)));
			mesureTimestamp.setSeconds(Integer.parseInt(m.group(3)));
			measure.put(SensorsDataKeeper.Date, mesureTimestamp.getTime());
			measure.put(SensorsDataKeeper.T, Float.parseFloat(m.group(4)));
			measure.put(SensorsDataKeeper.P, Float.parseFloat(m.group(5)));
			measure.put(SensorsDataKeeper.h, Float.parseFloat(m.group(6)));
		}
		catch (Exception e) 
		{
			return false;
		}

		sqdb.insert(db.tableName(), null, measure);
		return true;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public DisplayDetailFragment() 
	{
	}

	public void clearHistory()
	{
		sqdb.execSQL(String.format(SensorsDataKeeper.SQL_DELETE_ENTRIES, db.tableName()));
		sqdb.execSQL(String.format(SensorsDataKeeper.SQL_CREATE_ENTRIES, db.tableName()));
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
		if (getArguments().containsKey(ARG_ITEM_ID)) {

			Bundle args = getArguments();
			int id = Integer.parseInt(args.getString(ARG_ITEM_ID));
			mItem = BTItemContent.ITEM_MAP.get(id);
			if (mItem == null)
				return;

			db = new SensorsDataKeeper(MeteostationApp.appContext, mItem.device);
			sqdb = db.getWritableDatabase();

			intent = new Intent(getActivity(), IOService.class);
			intent.putExtra(IOService.DEV_MAC, mItem.device.getAddress());

			sConn = new ServiceConnection() {

				@Override
				public void onServiceConnected(ComponentName name, IBinder binder) {
					DisplayDetailFragment.this.binder = (IOService.IOServiceBinder)binder;
					bound = true;
				}

				@Override
				public void onServiceDisconnected(ComponentName arg0) {
					// TODO: ����� ����� � ��������
					bound = false;
				}
			};
		}
		getActivity().bindService(intent, sConn, Activity.BIND_AUTO_CREATE);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_menu, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onDestroy() {
		if (bound)
			getActivity().unbindService(sConn);
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_sensor_detail,
				container, false);


		if (mItem != null) 
		{
			((TextView) rootView.findViewById(R.id.sensor_name))
			.setText(mItem.device.getName());
			refreshView();
		}

		((Button)rootView.findViewById(R.id.refresh_button)).setOnClickListener(refrashClickListener);

		return rootView;
	}

	@SuppressWarnings("unused")
	private void makeToast(String Text)
	{
		Toast.makeText(getActivity(), Text, Toast.LENGTH_SHORT).show();
	}

	private void makeToast(int Textid)
	{
		Toast.makeText(getActivity(), Textid, Toast.LENGTH_SHORT).show();
	}

	private void DisplayNoConnectionAvalable()
	{
		makeToast(R.string.No_connection);
	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {	
		List<String> ansver;
		boolean sucess = false;
		switch (item.getItemId()) {
		case R.id.clear_menuitem:
			clearHistory();
			refreshView();
			sucess = true;
			break;
		case R.id.refrash_menuitem:
			refrashClickListener.onClick(null);
			break;
		case R.id.syncDateTime_menuitem:
			ansver = binder.processCommand(
					"date " + 
							(new SimpleDateFormat("MMddHHmmyy.ss")).format(new Date()));
			if (ansver != null && ansver.contains("Date updated"))
			{
				makeToast(R.string.Date_updated);
				sucess = true;
			}
			break;
		case R.id.takeMesure_menuitem:
			ansver = binder.processCommand("takemesure");
			if (ansver != null && ansver.contains("Saved"))
			{
				makeToast(R.string.Saved_successfuly);
				sucess = true;
			}
			break;
		case R.id.StartSeriesWrite_menuitem:
			ansver = binder.processCommand("wvariants");
			if (ansver != null && !ansver.isEmpty())
			{
				Matcher m = variantsPattern.matcher(ansver.get(0));
				if (!m.find())
					break;

				final int[] v;
				int selected;
				try 
				{
					v = new int[] { 	
							Integer.parseInt(m.group(1)), 
							Integer.parseInt(m.group(2)),
							Integer.parseInt(m.group(3))
					};
					selected = Integer.parseInt(m.group(4));
				}
				catch (NumberFormatException e)
				{
					break;
				}

				// ���������
				if (v[0] < 1 || v[0] > 0xffff ||
						v[1] < 1 || v[1] > 0xffff ||
						v[2] < 1 || v[2] > 0xffff ||
						selected < 0 || selected > 2)
					break;

				final String[] variantsText = new String[3];

				final Resources r = getResources();

				for (int i = 0; i < 3; ++i)
					variantsText[i] = String.valueOf(v[i]) + " " + r.getString(R.string.seconds);

				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder
				.setTitle(R.string.IntervalSelector_title)
				.setCancelable(true)
				.setSingleChoiceItems(variantsText, selected, null)
				.setPositiveButton(R.string.IntervalSelector_Start, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						int itemSelected = 
								((AlertDialog)dialog).getListView().getCheckedItemPosition();

						if (itemSelected > -1 && itemSelected < 3)
						{
							List<String> ansver = binder.processCommand("recstart " + 
									itemSelected + " " + v[itemSelected]);
							if (ansver != null && ansver.contains("Saving started..."))
								makeToast(R.string.Recording_begins);
							else
								makeToast(R.string.Recording_fails);
						}
					}
				})
				.setNegativeButton(android.R.string.cancel, null);
				final AlertDialog ad = builder.create();
				ad.setOnShowListener(new OnShowListener() 
				{       
					@Override
					public void onShow(DialogInterface dialog) 
					{       
						final ListView lv = ad.getListView(); //this is a ListView with your "buds" in it
						lv.setOnItemLongClickListener(new OnItemLongClickListener() 
						{
							@Override
							public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) 
							{
								final EditText input = new EditText(lv.getContext());
								input.setText(String.valueOf(v[position]));
								
								// ������ �����
								input.setFilters(new InputFilter[] {
									    // Maximum 5 characters.
									    new InputFilter.LengthFilter(5),
									    // Digits only.
									    DigitsKeyListener.getInstance(),  // Not strictly needed, IMHO.
									});
								input.setKeyListener(DigitsKeyListener.getInstance());
								
								AlertDialog.Builder builder1 = new AlertDialog.Builder(lv.getContext());
								builder1
								.setTitle(R.string.IntervalEdit_title)
								.setCancelable(true)
								.setView(input)
								.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										v[position] = Integer.parseInt(input.getText().toString());
										String[] variantsText1 = new String[v.length];
 										for (int i = 0; i < 3; ++i)
											variantsText1[i] = String.valueOf(v[i]) + " " + r.getString(R.string.seconds);
										
										ArrayAdapter<CharSequence> adapter = 
												new ArrayAdapter<CharSequence>(lv.getContext(), 
														android.R.layout.select_dialog_singlechoice, 
														android.R.id.text1, variantsText1);
							            
										lv.setAdapter(adapter);
									}
								})
								.setNegativeButton(android.R.string.cancel, null);
								builder1.create().show();
								return true;
							}           
						});     
					}
				});
				ad.show();

				sucess = true;
			}
			break;
		case R.id.CancelRecording_menuitem:
			ansver = binder.processCommand("wstop");
			if (ansver != null && ansver.contains("Recording canceled"))
			{
				makeToast(R.string.Recording_stopped);
				sucess = true;
			}
			break;
		case R.id.ViewHistory_menuitem:
		{
			Intent intent = new Intent(getActivity(), FileListActivity.class);
			intent.putExtra("MAC", mItem.device.getAddress());
			startActivity(intent);
			sucess = true;
			break;
		}	
		default:
			return super.onOptionsItemSelected(item);
		}
		if (!sucess)
			DisplayNoConnectionAvalable();

		return true;
	}

	public void refreshView()
	{
		Resources res = getResources();
		Cursor cursor;
		try
		{
			cursor = sqdb.query(db.tableName(), new String[] {
				SensorsDataKeeper.UID, SensorsDataKeeper.Date,
				SensorsDataKeeper.P, SensorsDataKeeper.T, SensorsDataKeeper.h
			}, 
			null, // The columns for the WHERE clause
			null, // The values for the WHERE clause
			null, // don't group the rows
			null, // don't filter by row groups
			null // The sort order
					);
		} 
		catch (SQLException e) 
		{
			clearHistory();
			cursor = sqdb.query(db.tableName(), new String[] {
				SensorsDataKeeper.UID, SensorsDataKeeper.Date,
				SensorsDataKeeper.P, SensorsDataKeeper.T, SensorsDataKeeper.h
			}, 
			null, // The columns for the WHERE clause
			null, // The values for the WHERE clause
			null, // don't group the rows
			null, // don't filter by row groups
			null // The sort order
					);
		}

		int[] columnIndexs = new int[] {
				cursor.getColumnIndex(SensorsDataKeeper.Date),
				cursor.getColumnIndex(SensorsDataKeeper.P),
				cursor.getColumnIndex(SensorsDataKeeper.T),
				cursor.getColumnIndex(SensorsDataKeeper.h)
		};

		if (cursor.moveToLast())
		{
			// ���� ���� 1 �������
			((TextView)rootView.findViewById(R.id.pressure_label))
			.setText(String.format("%.2f", cursor.getFloat(columnIndexs[1])));
			((TextView)rootView.findViewById(R.id.temperature_label))
			.setText(String.format("%+.2f", cursor.getFloat(columnIndexs[2])));
			((TextView)rootView.findViewById(R.id.humidity_label))
			.setText(String.format("%.2f", cursor.getFloat(columnIndexs[3])));		
		}
		else
		{
			String na = res.getString(R.string.na);
			((TextView)rootView.findViewById(R.id.humidity_label)).setText(na);
			((TextView)rootView.findViewById(R.id.temperature_label)).setText(na);
			((TextView)rootView.findViewById(R.id.pressure_label)).setText(na);		
		}

		// ������ ������ �� ������ �� ����
		LinearLayout place = (LinearLayout)rootView.findViewById(R.id.graph);
		place.removeAllViews();
		place.addView(graph.buildGraph(cursor, res.getString(R.string.graph_title)));

		cursor.close();
	}
}
