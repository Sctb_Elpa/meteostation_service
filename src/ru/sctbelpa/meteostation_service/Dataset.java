package ru.sctbelpa.meteostation_service;

public class Dataset 
{
	public final long timestamp;
	public final float T;
	public final float P;
	public final float H;

	public Dataset() 
	{
		this.timestamp = 0;
		this.T = 0;
		this.P = 0;
		this.H = 0;
	}
	
	public Dataset(long timestamp, float T, float P, float H)
	{
		this.timestamp = timestamp;
		this.T = T;
		this.P = P;
		this.H = H;
	}
};