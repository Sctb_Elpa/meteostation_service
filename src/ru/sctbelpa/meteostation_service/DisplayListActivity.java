package ru.sctbelpa.meteostation_service;

import ru.sctbelpa.meteostation_service.BT.BTItemContent;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.ContextThemeWrapper;

/**
 * An activity representing a list of BTDevices. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link DisplayDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link DisplayListFragment} and the item details (if present) is a
 * {@link DisplayDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link DisplayListFragment.Callbacks} interface to listen for item
 * selections.
 */
public class DisplayListActivity extends FragmentActivity implements
		DisplayListFragment.Callbacks {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	private boolean mTwoPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sensor_list);

		if (findViewById(R.id.sensor_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((DisplayListFragment) getSupportFragmentManager()
					.findFragmentById(R.id.sensor_list))
					.setActivateOnItemClick(true);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		if (adapter == null)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(
					new ContextThemeWrapper(this, R.style.popup_theme));
			builder.setTitle(R.string.E_bluetooth_not_supported)
			.setMessage(R.string.E_bluetooth_not_supported_)
			.setCancelable(false)
			.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			})
			.create().show();
			return;
		}
		else
		{
			if (!adapter.isEnabled())
			{
				AlertDialog.Builder builder = new AlertDialog.Builder(
						new ContextThemeWrapper(this, R.style.popup_theme));
				builder.setTitle(R.string.bt_request)
				.setMessage(R.string.bt_request_desc)
				.setCancelable(false)
				.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Intent enBTintent = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
						startActivity(enBTintent);
					}
				})
				.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
					}
				});	
				builder.create().show();
			}
			else
			{
				adapter.cancelDiscovery();
								
				int i = 0;
				BTItemContent.clearItems();
				for (BluetoothDevice dev : adapter.getBondedDevices())
					BTItemContent.addItem(new BTItemContent.BTItem(i++, dev));
			}
		}
	}

	/**
	 * Callback method from {@link DisplayListFragment.Callbacks} indicating
	 * that the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(int id) {		
		// ����� ��������� ������� ����������
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(DisplayDetailFragment.ARG_ITEM_ID, "" + id);
			DisplayDetailFragment fragment = new DisplayDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.sensor_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, DisplayDetailActivity.class);
			detailIntent.putExtra(DisplayDetailFragment.ARG_ITEM_ID, "" + id);
			startActivity(detailIntent);
		}
	}
}
